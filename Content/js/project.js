﻿function getProjectType(id) {
    var x = "";
    switch (id) {
        case 0:
            x = "";
            break;
        case 1:
            x = "市政项目";
            break;
        case 2:
            x = "公建项目";
            break;
        case 3:
            x = "经营项目";
            break;
        case 4:
            x = "房建项目";
            break;
    }
    return x;
}

function getProjectProperty(id) {
    var x = "";

    switch (id) {
        case 0:
            x = "";
            break;
        case 1:
            x = "续建";
            break;
        case 2:
            x = "新建";
            break;
        case 3:
            x = "储备";
            break;
    }
    return x;
}

function getProjectState(id) {
    var x="";

    switch (id) {
        case 0:
            x = "";
            break;
        case 1:
            x = "立项分析";
            break;
        case 2:
            x = "中标";
            break;
        case 3:
            x = "项目前期";
            break;

        case 4:
            x = "落标";
            break;
        case 5:
            x = "放弃";
            break;
    }
    return x;
}

function getProjectRecent(id) {
    var x = "";

    switch (id) {
        case true:
            x = "是";
            break;
        case false:
            x = "否";
            break;
    }
    return x;
}
